<?php

namespace Drupal\paragraphs_report\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\paragraphs_report\ParagraphsReport;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Paragraphs Report controller.
 */
class ParagraphsReportController extends ControllerBase {

  /**
   * ParagraphsReport class object.
   *
   * @var \Drupal\paragraphs_report\ParagraphsReport
   */
  protected $paragraphsReport;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\paragraphs_report\ParagraphsReport $paragraphsReport
   *   Paragraphs report object.
   */
  public function __construct(ParagraphsReport $paragraphsReport) {
    $this->paragraphsReport = $paragraphsReport;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paragraphs_report.report')
    );
  }

  /**
   * Return a rendered table ready for output.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Return markup for report output.
   */
  public function showReport() {
    return $this->paragraphsReport->showReport();
  }

  /**
   * Batch API starting point.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return redirect after batch has completed.
   */
  public function batchRunReport() {
    $nids = $this->paragraphsReport->getNodes();
    // Get all nodes to process.
    // If no nodes, exit.
    if (!empty($nids)) {
      // Put nodes into batches.
      $batch = $this->paragraphsReport->batchPrep($nids);
      // Start batch api process.
      batch_set($batch);
      // Redirect page and display message on completion.
      return batch_process('/admin/reports/paragraphs-report');
    }
    else {
      $msg = 'No nodes found to process. Please update the report settings.';
      $this->getLogger('Paragraphs Report')->info($msg);
      $this->messenger()->addMessage($msg);
      return new RedirectResponse(Url::fromRoute('paragraphs_report.settings')->toString());
    }
  }

  /**
   * Return a CSV file of the rendered data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return the rendered csv.
   */
  public function exportReport() {
    return $this->paragraphsReport->exportReport();
  }

}
